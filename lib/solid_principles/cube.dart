
import 'package:solid_principles/solid_principles/shape.dart';
import 'package:solid_principles/solid_principles/three_dimensional_shape.dart';

class Cube implements Shape, ThreeDimensionalShape{
// Cube is a 3D so we can calculate its volume and area
// When we create a new ThreeDimensionalShape for cube to implement
// we solved the problem of the Interface segregation
  @override
  double area() {
    return 10;
  }

  @override
  double volume() {
    return 100;
  }

}