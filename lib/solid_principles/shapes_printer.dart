
import 'package:solid_principles/solid_principles/area_calculator.dart';
import 'package:solid_principles/solid_principles/shape.dart';

class ShapesPrinter {
  /*
  Here we will solve the Dependency Inversion
   */
  
  final IAreaCalculator areaCalculator;

  ShapesPrinter({required this.areaCalculator});

  String printJson(List<Shape> shapes) {
    return "Print Json : {'sum': '${areaCalculator.sum(shapes)}'}";
  }

  String printCSV(List<Shape> shapes) {
    return "Print csv : Sum = ${areaCalculator.sum(shapes)}";
  }
  
  
  /*
  These are to solve the Single Responsibility principle
  String printJson(double sum) {
    return "Print Json : {'sum': '$sum'}";
  }

  String printCSV(double sum) {
    return "Print csv : Sum = $sum}";
  }
  
   */
}