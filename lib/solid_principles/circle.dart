import 'package:solid_principles/solid_principles/shape.dart';
import 'dart:math' as math;

class Circle implements Shape{
  final double radius;

  Circle({required this.radius});

  @override
  double area() {
    return math.pi * math.pow(radius, 2);
  }
}