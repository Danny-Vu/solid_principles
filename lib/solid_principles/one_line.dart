import 'package:solid_principles/solid_principles/shape.dart';

class OneLine implements Shape {
  final double width;

  OneLine({required this.width});

  @override
  double area() {
    //This broke the Liskov Substitution principle
    throw Exception("No area here because Oneline does not have an area");
  }
}