import 'package:solid_principles/solid_principles/shape.dart';

abstract class IAreaCalculator {
  double sum(List<Shape> shapes);
}

class AreaCalculator extends IAreaCalculator {
  // This is a concrete class we need to create its abstractionn class
  // in order to solve the Dependency Inversion

  /*
  This function is broken the Open Closed principle
  because we tried to modify it by adding Cube

  double sum(List<dynamic> shapes) {
    double sum = 0;
    for (var shape in shapes) {
      if (shape is Square) {
        sum += math.pow(shape.length, 2);
      }
      if (shape is Circle) {
        sum += math.pi * math.pow(shape.radius, 2);
      }
      if (shape is Cube) {
        /*
        When we add cube here, we broke the Open Closed principle
        because we're trying to modify the sum function
        Solution: create a abstract class called Shape
         */
      }
    }
   */
  @override
  double sum(List<Shape> shapes) {
    double sum = 0;
    for (var shape in shapes) {
      sum += shape.area();
    }
    return sum;
  }

  /*
  This is a break of Single Responsibility ->
  should bring this function to a new class to only
  print it out

  String printJson(List<dynamic> shapes) {
    return "{'sum': '${sum(shapes)}'}";
  }

  String printCSV(List<dynamic> shapes) {
    return "Sum = ${sum(shapes)}";
  }
   */
}