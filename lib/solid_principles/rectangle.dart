import 'package:solid_principles/solid_principles/shape.dart';

class Rectangle implements Shape {
  final double width;
  final double height;

  Rectangle({required this.width, required this.height});

  @override
  double area() {
    return width * height;
  }

}