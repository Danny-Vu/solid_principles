import 'dart:math' as math;
import 'package:solid_principles/solid_principles/shape.dart';

class Square implements Shape {
  final int length;

  Square({required this.length});

  @override
  double area() {
    return double.tryParse("${math.pow(length, 2)}") ?? 0.0;
  }
}