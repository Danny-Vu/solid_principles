
abstract class Shape {
  double area();

  /*
  When we add volume hear -> we will break the Interface segregation principle
  Solution: create a new abstract class for calculating 3D area
  double volume();
   */
}