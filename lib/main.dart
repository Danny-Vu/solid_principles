import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:solid_principles/solid_principles/area_calculator.dart';
import 'package:solid_principles/solid_principles/area_calculator_v2.dart';
import 'package:solid_principles/solid_principles/circle.dart';
import 'package:solid_principles/solid_principles/cube.dart';
import 'package:solid_principles/solid_principles/one_line.dart';
import 'package:solid_principles/solid_principles/rectangle.dart';
import 'package:solid_principles/solid_principles/shape.dart';
import 'package:solid_principles/solid_principles/shapes_printer.dart';
import 'package:solid_principles/solid_principles/square.dart';

/*
SOLID PRINCIPLES

1. Single Responsibility
  Each class should have only one sole purple,
  and not be filled with excessive functionality.

2. Open Closed
  Classes should be open for extension,
  closed for modification.
  In other words, you should not have to rewrite
  an existing class for implementing new features.

3. Liskov Substitution
  This means that every subclass or derived class
  should be substitutable for their parent class.

4. Interface Segregation
  Interfaces should not force classes to implement
  what they can't do.
  Large interfaces should be divided into small ones.

5. Dependency Inversion
  Components should depend on abstractions,
  not on concretions.
 */

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  StatelessElement createElement() {
    //Todo
    IAreaCalculator areaCalculator = AreaCalculator();
    /*
    When we create another areaCalculatorV2, the benefits here are that
    we don't need to change anything insight ShapesPrinter() because both are depended on
    abstract class, not concrete class
     */
    IAreaCalculator areaCalculatorV2 = AreaCalculatorV2();
    ShapesPrinter shapesPrinter = ShapesPrinter(areaCalculator: areaCalculatorV2);
    Circle circle = Circle(radius: 10);
    Square square = Square(length: 10);
    Cube cube = Cube();
    Rectangle rectangle = Rectangle(width: 4, height: 3);
    OneLine oneLine = OneLine(width: 8);
    /*
    This sum broke the Open Closed principle
    List<dynamic> shapes = [circle, square];
    double sum = areaCalculator.sum(shapes);
     */

    /*
    When we add oneLine into the shapes we will break the Liskov Substitution
    List<Shape> shapes = [circle, square, cube, rectangle, oneLine];
     */
    List<Shape> shapes = [circle, square, cube, rectangle];
   // double sum = areaCalculator.sum(shapes);
    print(shapesPrinter.printJson(shapes));
    print(shapesPrinter.printCSV(shapes));

    return super.createElement();
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Container(
        color: Colors.green,
      ),
    );
  }
}
